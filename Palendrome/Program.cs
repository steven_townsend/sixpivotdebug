﻿using System;

namespace Palendrome
{
    class Program
    {
        //  Write a function that checks if a given sentence is a palindrome. 
        //  A palindrome is a word, phrase, verse, //  or sentence that reads the same backward or forward. 
        //  Only the order of English alphabet letters (A-Z and a-z) should be considered, 
        //  other characters should be ignored.

        //  For example, IsPalindrome("Never odd or even!") should return true as spaces, exclamations, and case should be ignored 
        //  resulting with "neveroddoreven" which is a palindrome since it reads same backward and forward.
        static void Main(string[] args)
        {
            Console.WriteLine(IsPalindrome("Never odd or even!"));
        }

        public static bool IsPalindrome(string str)
        {
            throw new NotImplementedException("Waiting to be implemented.");
        }
    }
}