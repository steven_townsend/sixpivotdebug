# README #

Six Pivot Debug and Interview challenges

Steps:

1. Branch from master
2. Open the solution
3. Unload WordWrangler (it does not build)
4. Do the LargestInt puzzle (instructions in Program.cs)
5. Do the Palendrome puzzle (instructions in Program.cs)
6. Reload WordWrangler
7. Do the WordWrangler puzzle

# WordWrangler Puzzle #

'Larry' shotgunned a change to the WOrdWrangler app before heading off on holidays. Not only does his change fail to fix the issue, the solution doesn't build. This app forms a critical part of the system and needs to be fixed ASAP!