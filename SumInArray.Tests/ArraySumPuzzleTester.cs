﻿using NUnit.Framework;
using Shouldly;

namespace SumInArray.Tests
{
    [TestFixture]
    public class ArraySumPuzzleTester {

        private ArraySumPuzzle _target;
	
        [OneTimeSetUp]
        public void SetUp(){
            _target = new ArraySumPuzzle();
        }

        [Test]
        [TestCase(new[] {1,2,3,4,6,9,11}, 8, true)]
        [TestCase(new[] {1,2,3,4,4,5,7,8,9,11}, 8, true)]
        [TestCase(new[] {2,3,4,4,8,9,11}, 8, true)]
        [TestCase(new[] {2,2,3,7,8,9,10}, 8, false)]
        [TestCase(new[] {2,3,4,7,8,9,10}, 8, false)]
        [TestCase(new[] {2,2,3,7,8,9,10}, 11, true)]
        [TestCase(new[] {1}, 1, false)]
        [TestCase(null, 1, false)]
        public void ContainsSumTest(int[] a, int targetSum, bool expectedResult){
            _target.ContainsSum(a, targetSum).ShouldBe(expectedResult);
        }
    }
}
